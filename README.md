# create api account

1. Sign up for a free PRECIRE account at https://precire.ai/signup
2. Verify your account by clicking on the link in the account verification email
3. Subscribe to the free product `Demo` at https://precire.ai/products

# local build & run

1. Get your primary or secondary key at https://precire.ai/developer
2. Insert your key into the `index.html` (testing only, please hide key from client)
3. Double click `index.html`